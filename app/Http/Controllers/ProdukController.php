<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::select('*')->get();
        return view('pages.produk.show',['produk' => $produk]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.produk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->gambar_produk;
        $filename = time() . "-produk-" . $file->getClientOriginalName();
        $uploadPath = "img/";
        $file->move($uploadPath, $filename);

        $produk = Produk::create([
            'nama_produk' => $request->nama_produk,
            'kategori_produk' => $request->kategori_produk,
            'gambar_produk' => $filename,
            'stok_produk' => $request->stok_produk,
            'harga_produk' => $request->harga_produk,
        ]);

        return redirect()->back()->with('success' ,'Berhasil Ditambah!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Produk::findOrFail($id);

        return view('pages.produk.edit',['item'=>$item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Produk::findOrFail($id);

        if (isset($request->gambar_produk)) {
            $file = $request->gambar_produk;
            $filename = time() . "-produk-" . $file->getClientOriginalName();
            $uploadPath = "img/";
            $file->move($uploadPath, $filename);

            $item->nama_produk = $request->nama_produk;
            $item->harga_produk = $request->harga_produk;
            $item->stok_produk = $request->stok_produk;
            $item->kategori_produk = $request->kategori_produk;
            $item->gambar_produk = $filename;
        } else {
            $item->nama_produk = $request->nama_produk;
            $item->harga_produk = $request->harga_produk;
            $item->stok_produk = $request->stok_produk;
            $item->kategori_produk = $request->kategori_produk;
        }
        $item->save();

        return redirect()->back()->with('success' ,'Berhasil Diedit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Produk::findOrFail($id);
        $item->delete();

        return redirect()->back()->with('success' ,'Berhasil Dihapus!');
    }
}
