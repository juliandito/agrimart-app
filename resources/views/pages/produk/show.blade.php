@extends('layouts.app')

@section('title','Produk')

@section('content')
<main class="sm:container sm:mx-auto sm:mt-10">
    <div class="w-full sm:px-6">
        <section class="flex flex-col break-words bg-white sm:border-1 sm:rounded-md sm:shadow-sm sm:shadow-lg">

            @if(Session::has('success'))
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
                <strong class="font-bold">Success!</strong>
                <span class="block sm:inline">{{Session::get('success')}}</span>
            </div>
            @endif

            <header class="font-semibold bg-gray-200 text-gray-700 py-5 px-6 sm:py-6 sm:px-8 sm:rounded-t-md">
                <div class="grid grid-cols-3 gap-4">
                    <div>
                        <h3>Produk AgriMart</h3> 
                    </div>
                    <div>
                        &nbsp;
                    </div>
                    <div class="text-right">
                        <a href="/admin/create">
                            <button 
                            class="w-24 select-none font-bold whitespace-no-wrap p-3 rounded-lg text-base leading-normal no-underline text-gray-100 bg-blue-400 hover:bg-blue-700 sm:py-4">
                            {{ __('Tambah') }}
                            </button>
                        </a>
                    </div>
                </div>
            </header>

            <div class="w-full p-6">
                <p class="text-gray-700">
                    <table id="produk-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Gambar Produk</th>
                            <th>Nama Produk</th>
                            <th>Kategori Produk</th>
                            <th>Stok Produk</th>
                            <th>Harga Produk</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($produk as $p)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td class="flex justify-center">
                                    <img src="/img/{{$p->gambar_produk}}" alt="" width="30%">
                                </td>
                                <td>{{$p->nama_produk}}</td>
                                <td>{{$p->kategori_produk}}</td>
                                <td>{{$p->stok_produk}}</td>
                                <td>{{$p->harga_produk}}</td>
                                <td>
                                    <a href="/admin/edit/{{$p->id}}">
                                        <button 
                                        class="w-full select-none font-bold whitespace-no-wrap p-3 rounded-lg text-base leading-normal no-underline text-gray-100 bg-green-400 hover:bg-green-700 sm:py-4">
                                        {{ __('Edit') }}
                                        </button>
                                    </a>
                                    <a href="/admin/delete/{{$p->id}}">
                                        <button 
                                        class="w-full select-none font-bold whitespace-no-wrap p-3 rounded-lg text-base leading-normal no-underline text-gray-100 bg-red-500 hover:bg-red-700 sm:py-4">
                                        {{ __('Delete') }}
                                        </button>
                                    </a>
                                </td>  
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </p>
            </div>
        </section>
    </div>
</main>

<script>
    $("#produk-table").DataTable({
        responsive: true, 
        lengthChange: true, 
        autoWidth:false,
        dom: 'frtip',
    });
</script>
@endsection
