@extends('layouts.app')

@section('title','Produk')

@section('content')
<main class="sm:container sm:mx-auto sm:max-w-lg sm:mt-10">
    <div class="flex">
        <div class="w-full">
            <section class="flex flex-col break-words bg-white sm:border-1 sm:rounded-md sm:shadow-sm sm:shadow-lg">
                @if(Session::has('success'))
                <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
                    <strong class="font-bold">Success!</strong>
                    <span class="block sm:inline">{{Session::get('success')}}</span>
                </div>
                @endif

                <header class="font-semibold bg-gray-200 text-gray-700 py-5 px-6 sm:py-6 sm:px-8 sm:rounded-t-md">
                    {{ __('Edit Produk') }}
                </header>

                <form class="w-full px-6 space-y-6 sm:px-10 sm:space-y-8" method="POST"
                    action="/admin/update/{{$item->id}}" enctype="multipart/form-data">
                    @csrf

                    <div class="flex flex-wrap">
                        <label for="name" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('Nama Produk') }}:
                        </label>
                        <input id="nama_produk" type="text" class="form-input w-full @error('name')  border-red-500 @enderror"
                            name="nama_produk" value="{{$item->nama_produk}}" required autocomplete="name" autofocus>
                    </div>

                    <div class="flex flex-wrap">
                        <label for="gambar" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('Gambar Produk') }}:
                        </label>
                        <img src="/img/{{$item->gambar_produk}}" alt="gambar-produk" width="30%">
                    </div>
                    <div class="flex flex-wrap">
                        <label for="gambar" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('Gambar Produk') }}:
                        </label>
                        <input id="gambar_produk" type="file" class="form-input w-full @error('gambar_produk')  border-red-500 @enderror"
                            name="gambar_produk" value="{{ old('gambar_produk') }}" autofocus>
                    </div>

                    <div class="flex flex-wrap">
                        <label for="kategori_produk" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('Kategori Produk') }}:
                        </label>
                        <select name="kategori_produk" id="kategori_produk" class="form-input w-full">
                        @if ($item->kategori_produk == 'Sayur')
                            <option value="Sayur" selected>Sayuran</option>
                            <option value="Buah">Buah - Buahan</option>
                        @else
                            <option value="Sayur">Sayuran</option>
                            <option value="Buah"selected>Buah - Buahan</option>
                        @endif
                        </select>
                    </div>

                    <div class="flex flex-wrap">
                        <label for="stok_produk" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('Stok Produk(kg)') }}:
                        </label>
                        <input id="stok_produk" type="number" class="form-input w-full @error('name')  border-red-500 @enderror"
                            name="stok_produk" value="{{$item->stok_produk}}" required autocomplete="name" autofocus>
                    </div>

                    <div class="flex flex-wrap">
                        <label for="harga_produk" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('Harga Produk(Rp)') }}:
                        </label>
                        <input id="harga_produk" type="number" class="form-input w-full @error('harga_produk')  border-red-500 @enderror"
                            name="harga_produk" value="{{$item->harga_produk}}" required autocomplete="harga_produk" autofocus>
                    </div>

                    <div class="flex flex-wrap">
                        <button type="submit"
                            class="w-full select-none font-bold whitespace-no-wrap p-3 rounded-lg text-base leading-normal no-underline text-gray-100 bg-blue-500 hover:bg-blue-700 sm:py-4">
                            {{ __('Simpan Perubahan') }}
                        </button>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </div>
                </form>

            </section>
        </div>
    </div>
</main>
@endsection
