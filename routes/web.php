<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/admin/produk', [ProdukController::class, 'index']);
Route::get('/admin/create', [ProdukController::class, 'create']);
Route::post('/admin/tambah', [ProdukController::class, 'store']);
Route::get('/admin/edit/{id}', [ProdukController::class, 'edit']);
Route::post('/admin/update/{id}', [ProdukController::class, 'update']);
Route::get('/admin/delete/{id}', [ProdukController::class, 'destroy']);
